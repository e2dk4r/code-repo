class profile::r10k {
    class { 'r10k':
        remote => 'git@gitlab.com:e2dk4r/code-repo.git',
    }

    class { 'r10k::webhook::config':
        use_mcollective => false,
        enable_ssl      => false,
    }

    class { 'r10k::webhook':
        user  => 'root',
        group => 'root',
    }
}

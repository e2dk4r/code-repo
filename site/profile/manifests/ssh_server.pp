class profile::ssh_server {
    package { 'openssh-server':
        ensure => present,
    }

    service { 'sshd':
        ensure => 'running',
        enable => 'true',
    }

    ssh_authorized_key { 'root@puppet':
        ensure => present,
        user   => 'root',
        type   => 'ssh-ed25519',
        key    => 'AAAAC3NzaC1lZDI1NTE5AAAAINzcet9rz2AwOUg+IGl3jIEfUBhvjwiw50CpSeLON4J9',
    }
}
